
The openid_provider_persona module provides Drupal with the ability too create / edit and manage
openid personas on your Drupal system.

The module is meant too be a helper module that will allow other modules such as Simple Registration 
and Attribute Exchange to call it in order to retrieve the attributes from the system for returning 
in the response to the user. 

Module provides two hooks that allow outside modules to provide the information for simple registration 
and attribute exchange attributes that 

hook_openid_provider_sreg - modules that implement this can return attributes for the simple registration 
parameters rather than taking them from the persona.

hook_openid_provider_ax - modules that implement this can return attributes for the attribute exchange 
parameters rather than taking them from the persona.

If the module has too use the saved personas it will attempt to verify the realm and determine the 
persona the user has specified for that realm. If nothing is specified the module will fallback to 
the default persona and return the data from that.

The module uses the schema definitions from http://www.axschema.org/types/

INSTALLATION
------------
Install and enable the OpenID Provider Persona  Drupal module as you would any Drupal module.

DEPENDENCIES
------------
Module relys on the OpenID Provider Module

Module is written purely for Drupal 6.x core.

CREDITS
-------
Idea taken and modified from http://drupal.org/project/openid_ax 
Developed and maintained by Darren Ferguson <darren.ferguson [at] openband [dot] net>
Sponsored by OpenBand <http://tech.openband.net/>
