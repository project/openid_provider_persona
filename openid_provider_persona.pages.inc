<?php
// $Id:

/**
 * Form for creating/editing a persona
 *
 * @param $form_state
 *      Initial form state
 * @param $user
 *      User object
 * @param $personas
 *      Persona we are adding / editing
 */
function openid_provider_persona_edit_form(&$form_state, $user = NULL, $personas = NULL) {
  if (!$user) {
    global $user;
  }

  $persona = $form = array();
  if (is_null($personas)) {
    $personas = 0;
  }

  $form['personas'] = array('#type' => 'hidden', '#value' => $personas);
  // Retrieving all of the current values we have stored for the persona we are currently viewing.
  $attribute_values = db_query("SELECT * FROM {openid_provider_persona_values} WHERE uid = %d and persona_id = '%d'", $user->uid, $personas);
  while ($row = db_fetch_array($attribute_values)) {
    $persona[$row['type_uri']] = $row;
  }

  // Build each of the form elements and make sure we put them in a field set.
  // Putting them in a fieldset will make sure the screen stays managable.
  foreach (openid_provider_persona_schema_definitions() as $key => $val) {
    // Build a field set so we can group the relevant fields together on the screen
    if (!isset($form[$val['section']])) {
      $form[$val['section']] = array('#type' => 'fieldset', '#title' => t($val['section']), '#collapsible' => TRUE, '#collapsed' => TRUE);
    }
    // Set the default value if the value exists in the persona array
    $default_values = (isset($persona[$key])) ? $persona[$key]['field_value'] : '';
    $form[$val['section']][$val['identifier']] = array(
      '#type' => 'textfield',
      '#title' => t($val['label']),
      '#description' => t($val['description']),
      '#default_value' => $default_values,
      '#size' => 25,
      '#maxlength' => 100
    );
  }

  // Saving the current persona values so we do not have to make another database
  // Request in order to retrieve them from the system in the submit handler
  $form['persona_values'] = array(
    '#type' => 'hidden',
    '#value' => serialize($persona),
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );
  return $form;
}

/**
 * Submit handler for openid_provider_persona_edit_form
 */
function openid_provider_persona_edit_form_submit(&$form, &$form_state) {
  global $user;

  $persona = $form_state['values']['personas'];
  // Data we serialized in the form creation function so we do not have to
  // Retrieve it again from the database in this function cut down processing
  $current_results = unserialize($form_state['values']['persona_values']);
  // Retrieve the attributes we are supporting and determine what has been set already
  foreach (openid_provider_persona_schema_definitions() as $key => $val) {
    // Checking to determine if the value exists and if it is not currently set in the persona values table.
    if (isset($form_state['values'][$val['identifier']]) && drupal_strlen($form_state['values'][$val['identifier']]) && !isset($current_results[$key])) {
      db_query("INSERT INTO {openid_provider_persona_values} (type_uri, uid, persona_id, field_value) VALUES ('%s', '%d', '%d', '%s')", $key, $user->uid, $persona, $form_state['values'][$val['identifier']]);
    }
    elseif (isset($form_state['values'][$val['identifier']]) && drupal_strlen($form_state['values'][$val['identifier']]) && isset($current_results[$key]) && $form_state['values'][$val['identifier']] != $current_results[$key]['field_value']) {
      // Checking if the value exists and it already exists in the persona values table and the value has changed
      db_query("UPDATE {openid_provider_persona_values} SET field_value = '%s' WHERE vid = '%d'", $form_state['values'][$val['identifier']], $current_results[$key]['vid']);
    }
    elseif (isset($form_state['values'][$val['identifier']]) && !drupal_strlen($form_state['values'][$val['identifier']]) && isset($current_results[$key])) {
      // Checking if the value has been removed from the Persona and if so we remove from the database
      db_query("DELETE FROM {openid_provider_persona_values} WHERE vid = '%d'", $current_results[$key]['vid']);
    }
  }
  drupal_set_message('Your persona has been saved.');
}

/**
 * Form for creating new persona
 *
 * @param $form_state
 *      State of the current form
 * @param $user
 *      User that the new persona is being created for
 */
function openid_provider_persona_create_form($form_state, $user) {
  $form = array();
  $form['persona'] = array(
    '#type' =>'textfield',
    '#title' => t('New Persona name'),
    '#default_value' => '',
    '#required' => TRUE,
    '#size' => 20
  );
  $form['user_id'] = array(
    '#type' => 'hidden',
    '#default_value' => $user->uid,
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );
  return $form;
}

/**
 * Submit handler for openid_provider_persona_create_form
 */
function openid_provider_persona_create_form_submit(&$form, $form_state) {

  if (db_result(db_query("SELECT * FROM {openid_provider_persona_persona} WHERE uid = %d AND persona_name = '%s'", $form_state['values']['user_id'], $form_state['values']['persona']))) {
    drupal_set_message(t('A persona with that name already exists. Please select another persona name.'), 'error');
  }
  else {
    $max_persona_id = db_result(db_query("SELECT MAX(persona_id) FROM {openid_provider_persona_persona} WHERE uid = '%d'", $form_state['values']['user_id']));
    $create_persona = db_query("INSERT INTO {openid_provider_persona_persona} (uid, persona_id, persona_name) VALUES ('%d', '%d', '%s')", $form_state['values']['user_id'], $max_persona_id + 1, $form_state['values']['persona']);
    if ($create_persona) {
      drupal_set_message(t('New Persona created'));
      $form_state['redirect'] = 'user/'. $form_state['values']['user_id'] .'/persona';
    }
  }
}

/**
 * Page to manage AX personas, viz, rename, delete, copy, etc.
 */
function openid_provider_persona_manage_personas($user = NULL) {
  if (!$user) {
    global $user;
  }

  // Header rows for the table that we are going to display to the user
  $header = array(t('Persona Name'), t('Edit Persona Values'), t('Rename Persona'), t('Copy Persona'), t('Delete Persona'));
  // Initial row that is always available since every user will always have a default persona
  $rows['default'] = array(
    t('Default'),
    l(t('Edit'), 'user/'. $user->uid .'/persona/edit/0'),
    t('-N/A-'),
    l(t('Copy'), 'user/'. $user->uid .'/persona/copy/0'),
    t('-N/A-')
  );

  // Retrieving additional Persona profiles that have been saved for the user in the system
  $profiles = db_query("SELECT * FROM {openid_provider_persona_persona} WHERE uid = %d", $user->uid);
  while ($profile = db_fetch_array($profiles)) {
    $rows[$profile['persona_name']] = array (
      $profile['persona_name'],
      l(t('Edit'), 'user/'. $user->uid .'/persona/edit/'. $profile['persona_id']),
      l(t('Rename'), 'user/'. $user->uid .'/persona/rename/'. $profile['persona_id']),
      l(t('Copy'), 'user/'. $user->uid .'/persona/copy/'. $profile['persona_id']),
      l(t('Delete'), 'user/'. $user->uid .'/persona/delete/'. $profile['persona_id']),
    );
  }
  // Returned the themed output to the user
  return theme('table', $header, $rows);
}

/**
 * Display a list of personas in the system
 */
function openid_provider_persona_copy_personas($user) {

  $personas = array();
  $personas_list = db_query("SELECT * FROM {openid_provider_persona_persona} WHERE uid = '%d'", $user->uid);
  while ($row = db_fetch_object($personas_list)) {
    $personas[$row->persona_id] = $row->persona_name;
  }

  // If greater than 0 then we have personas and hence can display them to the user.
  if (count($personas)) {
    $header = array(t('Name'), t('Copy'));
    $rows[] = array(t('Default'), l(t('Copy'), 'user/'. $user->uid .'/persona/copy/'. 0));
    foreach ($personas as $key => $value) {
      $rows[] = array($value, l(t('Copy'), 'user/'. $user->uid .'/persona/copy/'. $key));
    }
    return theme('table', $header, $rows);
  }

  return '<div>'. t('You only have your default persona hence you cannot copy from it.') .'</div>';
}

/**
 * Build the form with the variables we will be copying over to the new persona
 *
 * @param $user
 *      User object
 * @param $persona_id
 *      Persona id we wish to copy from
 */
function openid_provider_persona_copy_persona_form($form_state, $user, $persona_id) {

  $form = $values = $options = array();
  $form['user_id'] = array(
    '#type' => 'hidden',
    '#value' => $user->uid
  );

  $form['persona_id'] = array(
    '#type' => 'hidden',
    '#value' => $persona_id,
  );

  // Check if the persona id is not 0 which is the default then we can copy back to it
  // Default is not displayed in the user persona table hence we are forced to check for it
  if (is_numeric($persona_id) && $persona_id) {
    $options[0] = t('Default');
  }

  // Select all of the personas that are in the table and are not the one we are currently copying
  $personas = db_query("SELECT * FROM {openid_provider_persona_persona} WHERE uid = '%d' AND persona_id <> '%d'", $user->uid, $persona_id);
  while ($row = db_fetch_array($personas)) {
    $options[$row['persona_id']] = $row['persona_name'];
  }

  // Display message informing that they cannot copy if there is only the default profile available
  if (!count($options)) {
    $form['intro'] = array(
      '#type' => 'markup',
      '#value' => t('<b>You only have a default profile, you must create an additional profile in order to copy values</b>'),
    );
  }
  else {
    $form['copy_to'] = array(
      '#type' => 'select',
      '#title' => t('Copy To'),
      '#options' => $options,
      '#description' => t('Persona to copy the values too.'),
    );

    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Copy'),
    );

    // Retrieve the values that this persona already has stored since we do not want to display
    // any persona fields that do not have information in them since we would be copying blank information
    $persona_values = db_query("SELECT type_uri, field_value FROM {openid_provider_persona_values} WHERE uid = '%d' AND persona_id = '%d'", $user->uid, $persona_id);
    while ($row = db_fetch_array($persona_values)) {
      $values[$row['type_uri']] = $row['field_value'];
    }

    // Hidden form field with the values in it so we do not have to do another database query in the submit handler.
    $form['persona_values'] = array('#type' => 'hidden', '#value' => serialize($values));

    // Retrieve the attributes we are supporting and determine what has been set already
    foreach (openid_provider_persona_schema_definitions() as $key => $val) {
      if (isset($values[$key])) {
        $form[$val['identifier']] = array(
          '#type' => 'checkbox',
          '#return_value' => $key,
          '#title' => $val['label'],
          '#description' => $val['description']
        );
      }
    }
  }
  return $form;
}

/**
 * Submit handler that will copy the information from one persona to another.
 */
function openid_provider_persona_copy_persona_form_submit(&$form, $form_state) {

  $values = unserialize($form_state['values']['persona_values']);
  // Retrieve the attributes we are supporting and determine what has been set already
  foreach (openid_provider_persona_schema_definitions() as $key => $val) {
    // Check if it exists and if so check if the value is set and if so copy the value
    if (isset($form_state['values'][$val['identifier']]) && $form_state['values'][$val['identifier']]) {
      // Courtesy remove incase a value currently exists for this field in the persona we are copying too
      db_query("DELETE FROM {openid_provider_persona_values} WHERE uid = '%d' AND persona_id = '%d' AND type_uri = '%s'", $form_state['values']['user_id'], $form_state['values']['copy_to'], $key);
      db_query("INSERT INTO {openid_provider_persona_values} (type_uri, uid, persona_id, field_value) VALUES ('%s', '%d', '%d', '%s')", $key, $form_state['values']['user_id'], $form_state['values']['copy_to'], $values[$key]);
    }
  }
  drupal_set_message(t('Persona fields have been copied'));
  $form_state['redirect'] = 'user/'. $form_state['values']['user_id'] .'/persona';
}

/**
 * Form for renaming persona
 *
 * @param $form_state
 *      Initial form state
 * @param $user
 *      User object
 * @param $persona
 *      Persona we are renaming
 */
function openid_provider_persona_rename_persona_form($form_state, $user, $persona) {

  $form = array();
  $persona_name = db_result(db_query("SELECT persona_name FROM {openid_provider_persona_persona} WHERE persona_id = '%d' AND uid = '%d'", $persona, $user->uid));
  if ($persona_name) {
    $form['persona'] = array(
      '#type' => 'textfield',
      '#default_value' => $persona_name,
      '#size' => 15,
    );
    $form['persona_id'] = array(
      '#type' => 'hidden',
      '#value' => $persona,
    );
    $form['user_id'] = array(
      '#type' => 'hidden',
      '#value' => $user->uid,
    );
    $form['submit'] = array (
      '#type' => 'submit',
      '#value' => t('Rename'),
    );
  }
  return $form;
}

/**
 * Submit handler for openid_provider_persona_rename_persona_form
 */
function openid_provider_persona_rename_persona_form_submit(&$form, &$form_state) {
  // Updating the users persona name in the system
  db_query("UPDATE {openid_provider_persona_persona} SET persona_name = '%s' WHERE uid = '%d' AND persona_id = '%d'", $form_state['values']['persona'], $form_state['values']['user_id'], $form_state['values']['persona_id']);
  drupal_set_message(t('Persona has been renamed'));
  $form_state['redirect'] = 'user/'. $form_state['values']['user_id'] .'/persona';
}

/**
 * Function for deleting persona
 *
 * @param $user
 *      User object
 * @param $persona
 *      Persona we are deleting
 */
function openid_provider_persona_persona_delete($user, $persona) {

  $persona_name = db_result(db_query("SELECT persona_name FROM {openid_provider_persona_persona} WHERE persona_id = '%d' AND uid = '%d'", $persona, $user->uid));
  $link = l(t($persona_name), 'user/'. $user->uid .'/persona/edit/'. $persona);
  $content = t('Are you sure you want to delete your !profile profile.', array('!profile' => $link));
  $content .= t(' This cannot be reversed and all your profile related details will be lost.');
  $content .= drupal_get_form('openid_provider_persona_persona_delete_confirm_form', $user, $persona);
  return $content;
}

/**
 * Form for confirming if the user wants to delete the persona
 *
 * @param $form_state
 *      Initial form state
 * @param $user
 *      User object
 * @param $persona
 *      Persona we are deleting
 */
function openid_provider_persona_persona_delete_confirm_form($form_state, $user, $persona) {
  $form = array ();
  $form['persona_id'] = array(
    '#type' => 'hidden',
    '#value' => $persona,
  );
  $form['user_id'] = array(
    '#type' => 'hidden',
    '#value' => $user->uid,
  );
  $form['delete'] = array(
    '#type' => 'submit',
    '#value' => t('Delete'),
  );
  $form['cancel'] =array(
    '#type' => 'submit',
    '#value' => t('Cancel'),
    '#submit' => array ('openid_provider_persona_persona_delete_confirm_form_cancel'),
  );
  return $form;
}

/**
 * Submit handler for openid_provider_persona_persona_delete_confirm_form
 */
function openid_provider_persona_persona_delete_confirm_form_submit($form, &$form_state) {

  $delValues = db_query("DELETE FROM {openid_provider_persona_values} WHERE uid = '%d'  AND persona_id = '%d'", $form_state['values']['user_id'], $form_state['values']['persona_id']);
  $deletePersona = db_query("DELETE FROM {openid_provider_persona_persona} WHERE uid = '%d' AND persona_id = '%d'", $form_state['values']['user_id'], $form_state['values']['persona_id']);
  if ($delValues && $deletePersona) {
    drupal_set_message(t('Persona deleted.'));
    drupal_goto('user/'. $form_state['values']['user_id'] .'/persona');
  }
}

/**
 * Cancel handler for openid_provider_persona_persona_delete_confirm_form
 */
function openid_provider_persona_persona_delete_confirm_form_cancel($form, $form_state) {
  drupal_set_message(t('Persona Deletion has been cancelled'));
  $form_state['redirect'] = 'user/'. $form_state['values']['user_id'] .'/persona';
}
